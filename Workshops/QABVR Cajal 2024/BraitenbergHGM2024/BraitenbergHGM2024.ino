#include <MKRMotorCarrier.h>
//#define INTERRUPT_PIN 6

// use first channel of 16 channels (started from zero)
//#define LEDC_CHANNEL_0         0
//#define LEDC_CHANNEL_SUM_1     1
//#define LEDC_CHANNEL_SUM_2     2
//#define LEDC_CHANNEL_F1        3
//#define LEDC_CHANNEL_F2        4


// ----------------------------- Summation Pins -----------------------------------//
//#define E1aPin                  12
//#define E1bPin                  27
//#define E1cPin                  15
//#define PWM_SUM_1_Pin            5 
//#define E2aPin                  A0
//#define E2bPin                  A1
//#define E2cPin                  A5
//#define PWM_SUM_2_Pin           18


#define MinThresholdPin            15
#define MaxThresholdPin            A5

// −−−−−−−−−−−−−−−−−− HARDWARE MAPPING CONSTANTS −−−−−−−−−−−−−−−−−−//
// −−−−−−−−−−−---- Motor (Excitatory / Inhibitory) Pins −−−−−−--−−−−−− //
#define RightMotorExcitatoryPin    A3
#define LeftMotorExcitatoryPin     32
#define RightMotorInhibitoryPin    A2
#define LeftMotorInhibitoryPin     33

 
// −−−−−−−−−−−-------------- Bumper Pins −----------------−−−−−−−−−−−− //
#define RightBumperPin          17
#define LeftBumperPin           21

// −−−−−−−−−−−--------------- Batery Pin ---------------−−−−−−−−−-−−−− //
#define bateryVoltagePin        13

// −−−−−−−−−−−−−−−−−− CALIBRATION CONSTANTS −−−−−−−−−−−−−−−-−−−−- // 
#define LeftMaxSpeed       600 //direct: 0..100[30] 
#define RightMaxSpeed      600 //30
#define LeftMinSpeed       10 //20
#define RightMinSpeed      10 //20
#define LeftMaxAcc         40000
#define RightMaxAcc        40000


// Min and Max values according to which the sensor inputs will be normalized
int SensorMaxValue = 4096; //direct: 0..4096[4096] 
int SensorMinValue = 20; //100

// −−−−−−−−−−−---------- Excitation / Inhibition -------−−−−−−−−−−−−−− //
int leftMotorInhibition;    // left motor inhibitory input value
int rightMotorInhibition;   // right motor inhibitory input value
int leftMotorExcitation;    // left motor excitatory input value
int rightMotorExcitation;   // right motor excitatory input value

int rightMotorCommand;      
int leftMotorCommand;

// −−−−−−−−−−−−−−−--------- Bumper State -------------−−−−−−−−−−−−−− //
int leftBumperState;
int rightBumperState;

// −−−−−−−−−−−−−----------- Motor Drive ------------−−−−−−−−−−−−−−−− //
int leftMotorDrive; 
int rightMotorDrive;

// −−−−−−−−−−−−−−−−−−−−− INITIALIZATION −−−−−−−−−−−−−−−−−−−−−− //

int batteryVoltage;     // Variable to store the battery voltage
long elapsedTime = 0;     // total life time of the robot since it is turned on (in ms)
long lastBeatTime = 0;    // time since last heart beat (in ms)
bool ledState = 1;        // used to display the hart beat


// setup fuction: called once at the start in every reset/powercycle.
void setup() {
  
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off (LOW is the voltage level)
  initPinModes();                   // init pin modes
  initSerialPort();                 // init serial port
  initMotorController();            // init motor controller
  digitalWrite(LED_BUILTIN, HIGH);  // turn heartbeat LED on
  lastBeatTime = millis();          // store current heart beat time

}



// −−−−−−−−−−−−−−−−−−−−−−−− CONTROL LOOP −−−−−−−−−−−−−−−−−−−−−−−−− //
void loop ()
{
  // Compute loop timing and blink led accordingly
  heartBeat();

  // Read sensor values 
  readSensors();

  // Read thresholding values
  readSensoryGains();

  // Escape Reflex
  if (leftBumperState)
    escapeReflex (LeftBumperPin);
  else
    if (rightBumperState)
      escapeReflex (RightBumperPin);


  // Integrate excitation and inhibition sensory information  
  sensorimotorMap();
  
  // drive motors
  driveMotors ( leftMotorDrive , rightMotorDrive );

  // Write status to the serial port
  writeSerial(); 
}


void sensorimotorMap()
{
  
  // Motor drive equals excitation supressed by inhibition value
  leftMotorDrive = leftMotorExcitation - leftMotorInhibition;
  rightMotorDrive = rightMotorExcitation - rightMotorInhibition;

  // prevent motors from driving backwards
  //leftMotorDrive = leftMotorDrive < 0 ? 0 : leftMotorDrive;
  //rightMotorDrive = rightMotorDrive < 0 ? 0 : rightMotorDrive;

  // normalize the motor drive values within the min and max sensory band values
  leftMotorDrive = mapWithDeadBand (leftMotorDrive, SensorMinValue, SensorMaxValue, LeftMinSpeed, LeftMaxSpeed);
  rightMotorDrive = mapWithDeadBand (rightMotorDrive, SensorMinValue, SensorMaxValue, RightMinSpeed, RightMaxSpeed);

}


void heartBeat()
{

  // get time since last heart beat
  elapsedTime = millis() - lastBeatTime;
  
  // if elapsed time > 500ms change the state of the LED
  if( elapsedTime > 500)
  {
    ledState = !ledState;
    lastBeatTime = millis();
    digitalWrite(LED_BUILTIN, ledState);
  }

}


void readSensors()
{
  
  // Read sensor values attached to the motor signals
  leftMotorExcitation = analogRead ( LeftMotorExcitatoryPin ) ; 
  rightMotorExcitation = analogRead ( RightMotorExcitatoryPin ) ; 
  leftMotorInhibition = analogRead ( LeftMotorInhibitoryPin ) ; 
  rightMotorInhibition = analogRead ( RightMotorInhibitoryPin ) ;


  // Read bumper state 
  leftBumperState = digitalRead ( LeftBumperPin ) ; 
  rightBumperState = digitalRead ( RightBumperPin ) ;

}

void readSensoryGains()
{
  
  // Read sensory bands
  int minValue = analogRead ( MinThresholdPin ) ; 
  int maxValue = analogRead ( MaxThresholdPin ) ; 

  // Modify the band min and max values if their difference is at least 100 units large
  if(minValue < (maxValue - 100))
  {
    SensorMinValue = minValue;
    SensorMaxValue = maxValue;
  }

}

void initSerialPort()
{
  // init serial port
  Serial.begin(115200);
}


void initPinModes()
{
  // set pin modes
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LeftBumperPin, INPUT);
  pinMode(RightBumperPin, INPUT);
}


void initMotorController()
{
  
  // attempt to connect to MKR motor controller
  if (!controller.begin())
    {
      // Couldn't connect! Is the red led blinking? You may need to update the firmware with FWUpdater sketch
      while (1);
    }

  // Reboot the motor controller; brings every value back to default
  controller.reboot();

  M1.setDuty(0);
  M2.setDuty(0); 
  
  // set PID to velocity control
  pid1.setControlMode(CL_VELOCITY);
  pid2.setControlMode(CL_VELOCITY);
  
  // set PID gains
  pid1.resetGains();
  pid2.resetGains();
  pid1.setGains(60, 10, 0); //50, 100, 0
  pid2.setGains(60, 10, 0);      

  // set PID max acceleration
  pid1.setMaxAcceleration(LeftMaxAcc);
  pid2.setMaxAcceleration(RightMaxAcc);

}




int mapWithDeadBand(int value,int inputMin,int inputMax,int outMin, int outMax)
{
  // Check if input is in the dead band
  if ( (-inputMin <= value) && (value <= inputMin))
    return 0;
  else  
  
  // Scale input to output:
  {
    if (value > 0)
      return map(value, inputMin, inputMax, outMin, outMax);
    else
      return map(value, -inputMax, -inputMin, -outMax, -outMin);
  }

}

//  updates the drive on  the left and right motors
void  driveMotors ( int leftDrive , int rightDrive)
{
      
//      leftMotorCommand = -leftDrive * 10;
//      rightMotorCommand = rightDrive * 10;
//      M1.setDuty(-leftDrive);
//     M2.setDuty(rightDrive);

      
      leftMotorCommand = -leftDrive;
      rightMotorCommand = rightDrive;
      pid1.setSetpoint(TARGET_VELOCITY, leftMotorCommand);
      pid2.setSetpoint(TARGET_VELOCITY, rightMotorCommand);

}


      
// activates the escape reflex , given a bumper 
void escapeReflex (int bumperPin )
{
  //  drive back for some amount of time
  driveMotors(-LeftMaxSpeed , -RightMaxSpeed ) ;
  delay (500);

  //  if the left bumper  was activated , turn  to  the right
  if ( bumperPin == LeftBumperPin )
  {
    driveMotors(LeftMaxSpeed , -RightMaxSpeed ) ;
  }
  // if the right bumper was activated , turn to the left
  if ( bumperPin == RightBumperPin )
  {
    driveMotors(-LeftMaxSpeed , RightMaxSpeed ) ;
  }
  //  wait for turn to finish
  delay (300);
}

void writeSerial()
{
  Serial.print("LM+:");
  Serial.print(leftMotorExcitation);
  Serial.print("\t");
  Serial.print("RM+:"); 
  Serial.print(rightMotorExcitation);
  Serial.print("\t");
  
  Serial.print("LM-:");
  Serial.print(leftMotorInhibition);
  Serial.print("\t");
  Serial.print("RM-:");
  Serial.print(rightMotorInhibition);
  Serial.print("\t"); 

  Serial.print("mB:");
  Serial.print(SensorMinValue);
  Serial.print("\t");
  Serial.print("MB:");
  Serial.print(SensorMaxValue);
  Serial.print("\t"); 

  Serial.print("LMotor:");
  Serial.print(leftMotorCommand);
  Serial.print("\t"); 
  Serial.print("RMotor:");
  Serial.print(rightMotorCommand);
  Serial.println();


}

float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

bool testBattery()
{
  float batteryVoltage = (float)battery.getConverted();
  
  //Reset to the default values if the battery level is lower than 11V
  if (batteryVoltage < 3) 
  {
    //  WARNING: LOW BATTERY ALL SYSTEMS DOWN
    M1.setDuty(0);
    M2.setDuty(0);
    M3.setDuty(0);
    M4.setDuty(0);
    return false;
  } else 
    return true;
}


int sign(int val)
{
  return (val < 0) ? -1 : 1;
}
