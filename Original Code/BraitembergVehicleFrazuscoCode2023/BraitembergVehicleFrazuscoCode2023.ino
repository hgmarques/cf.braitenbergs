#include <MKRMotorCarrier.h>
//#define INTERRUPT_PIN 6

// use first channel of 16 channels (started from zero)
#define LEDC_CHANNEL_0         0
#define LEDC_CHANNEL_SUM_1     1
#define LEDC_CHANNEL_SUM_2     2
#define LEDC_CHANNEL_F1        3
#define LEDC_CHANNEL_F2        4

// −−−−−−−−−−−−−−−−−− HARDWARE MAPPING CONSTANTS −−−−−−−−−−−−−−−−−−//
// −−−−−−−−−−−---- Motor (Excitatory / Inhibitory) Pins −−−−−−--−−−−−− //
#define RightMotorExcitatoryPin    A3
#define LeftMotorExcitatoryPin     32
#define RightMotorInhibitoryPin    A2
#define LeftMotorInhibitoryPin     33

// ----------------------------- Sommation Pins -----------------------------------//
#define E1aPin                  12
#define E1bPin                  27
#define E1cPin                  15
#define PWM_SUM_1_Pin            5 
 
#define E2aPin                  A0
#define E2bPin                  A1
#define E2cPin                  A5
#define PWM_SUM_2_Pin           18
// ----------------------------- Function Pins -----------------------------------//
#define F1_IN_Pin               14
#define PWM_F1_OUT_Pin          19
#define F2_IN_Pin               A4
#define PWM_F2_OUT_Pin          16

 
// −−−−−−−−−−−-------------- Bumper Pins −----------------−−−−−−−−−−−− //
#define RightBumperPin          17
#define LeftBumperPin           21

// −−−−−−−−−−−--------------- Batery Pin ---------------−−−−−−−−−-−−−− //
#define bateryVoltagePin        13
// use 13 bit precission for LEDC timer
#define LEDC_TIMER_13_BIT       13

// use 5000 Hz as a LEDC base frequency
#define LEDC_BASE_FREQ          5000
  

// −−−−−−−−−−−−−−−−−− CALIBRATION CONSTANTS −−−−−−−−−−−−−−−-−−−−- // 
// −−------ VARY THE MAXIMUM VALUES TO GET THE ROBOT TO GO STRAIGHT ------−−-- //
// --------- DIRECT: 0..100[30]    PID:0..10000[200]

#define LeftMaxSpeed       35 //direct: 0..100[30] 
#define RightMaxSpeed      35 //30
#define LeftMinSpeed       10 //20
#define RightMinSpeed      10 //20

#define SensorMaxValue     4096 //direct: 0..4096[4096] 
#define SensorMinValue     20 //100

#define NUMBER_OF_BEHAVIORS 3

// −−−−−−−−−−−−−−−−−−−−− SENSOR VARIABLES −−−−−−−−−−−−−−−−−−−−− //
// −−−−−−−−−−−---------- Excitation / Inhibition -------−−−−−−−−−−−−−− //
int leftMotorInhibition; 
int rightMotorInhibition; 
int leftMotorExcitation; 
int rightMotorExcitation;

int rightMotorCommand;
int leftMotorCommand;
// −−−−−−−−−−−−−−−--------- Bumper State -------------−−−−−−−−−−−−−− //
int leftBumperState;
int rightBumperState;

// −−−−−−−−−−−−−−−−−−−− MOTOR VARIABLES −−−−−−−−−−−−−−−−−−−−−−− //
// −−−−−−−−−−−−−----------- Motor Drive ------------−−−−−−−−−−−−−−−− //
int leftMotorDrive; 
int rightMotorDrive;
// −−−−−−−−−−−−−−−−−−− FUNCTION VARIABLES −−−−−-−−−−−−−−−−−−−−−− //
// −−−−−−−−−−−−−------------ Summation  ------------−−−−−−−−−−−−−−−− //
int E1a       = 0;
int E1b       = 0; 
int E1c       = 0;
int PWM_SUM_1 = 0;
 
int E2a       = 0;
int E2b       = 0;
int E2c       = 0;
int PWM_SUM_2 = 0;
// ----------------------------- Function Pins -----------------------------------//
// −−−−−−−−−−−−−------------- Oscilator ------------−−−−−−−−−−−−−−−− //
int F1_IN  = 0;
int PWM_F1 = 0;
float temporalBaseRealtime1 = 0;
int F2_IN  = 0;
int PWM_F2 = 0;
float temporalBaseRealtime2 = 0;
// ----------------------------- Behavior -----------------------------------------//
int i = 0;
void (*myBehavior[NUMBER_OF_BEHAVIORS])();
float bProb[NUMBER_OF_BEHAVIORS];
int currentAction = 0;
unsigned long millisAction = 0;

// −−−−−−−−−−−−−−−−−−−−− INITIALIZATION −−−−−−−−−−−−−−−−−−−−−− //
// Loop time
float MillisLast = 0;

//Variable to store the battery voltage
int batteryVoltage;

// Arduino like analogWrite
// value has to be between 0 and valueMax
void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
  // calculate duty, 8191 from 2 ^ 13 - 1
  uint32_t duty = (8191 / valueMax) * min(value, valueMax);

  // write duty to LEDC
  ledcWrite(channel, duty);
}

// setup fuction: called once at the start in every reset/powercycle.
void setup() {
  
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
   
  // Setup timer and attach timer to a led pin
  
  ledcSetup(LEDC_CHANNEL_0, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcSetup(LEDC_CHANNEL_SUM_1, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcSetup(LEDC_CHANNEL_SUM_2, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcSetup(LEDC_CHANNEL_F1, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcSetup(LEDC_CHANNEL_F2, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcAttachPin(LED_BUILTIN, LEDC_CHANNEL_0);
  ledcAttachPin(PWM_SUM_1, LEDC_CHANNEL_SUM_1);
  ledcAttachPin(PWM_SUM_2, LEDC_CHANNEL_SUM_2);
  ledcAttachPin(PWM_F1_OUT_Pin, LEDC_CHANNEL_F1);
  ledcAttachPin(PWM_F2_OUT_Pin, LEDC_CHANNEL_F2);

    // make the pushbutton's pin an input:
  pinMode(LeftBumperPin, INPUT);
  pinMode(RightBumperPin, INPUT);

  //Establishing the communication with the motor shield
  if (!controller.begin())
    {
      // Couldn't connect! Is the red led blinking? You may need to update the firmware with FWUpdater sketch
      while (1);
    }

  // Reboot the motor controller; brings every value back to default
  controller.reboot();

  M1.setDuty(0);
  M2.setDuty(0); 
  
  pid1.setControlMode(CL_VELOCITY);
  pid2.setControlMode(CL_VELOCITY);
  pid1.resetGains();
  pid2.resetGains();
  
  pid1.setGains(60, 10, 0); //50, 100, 0
  pid2.setGains(60, 10, 0);      
      
  pid1.setLimits(-100, 100);
  pid2.setLimits(-100, 100);
      
  pid1.setMaxAcceleration(40000);
  pid2.setMaxAcceleration(40000);

  
  delay(500);

  random(1000);


  //Take the battery status
  float batteryVoltage = (float)battery.getConverted();   
  Serial.begin(9600);
  delay (500);
}

// −−−−−−−−−−−−−−−−−−−−−−−− CONTROL LOOP −−−−−−−−−−−−−−−−−−−−−−−−− //
void loop ()
{
  // loop clock time for realtime functions
  unsigned long MillisNow = millis();
  unsigned long stepMillis = MillisNow - MillisLast;
  MillisLast = MillisNow;


  // Read Left parcels and output left Sum
  E1a       = analogRead (E1aPin);
  E1b       = analogRead (E1bPin);
  E1c       = analogRead (E1cPin);
  PWM_SUM_1 = E1a + E1b + E1c;
  PWM_SUM_1 = map(PWM_SUM_1,0,3*4096,0,511);
  ledcAnalogWrite(LEDC_CHANNEL_SUM_1, PWM_SUM_1,511);

  // Read Right parcels and output Right Sum
  E2a       = analogRead (E2aPin);
  E2b       = analogRead (E2bPin);
  E2c       = analogRead (E2cPin);
  PWM_SUM_2 = E2a + E2b + E2c;
  PWM_SUM_2 = map(PWM_SUM_2,0,3*4096,0,511);
  ledcAnalogWrite(LEDC_CHANNEL_SUM_2, PWM_SUM_2,511);

  // Function Generator Left (saw)
  F1_IN = analogRead(F1_IN_Pin);
  temporalBaseRealtime1 = fmod(temporalBaseRealtime1 + ((F1_IN*stepMillis)/4096.0f),200.0*PI);
  ledcAnalogWrite(LEDC_CHANNEL_F1, temporalBaseRealtime1, 200.0*PI);
  servo1.setAngle(map(temporalBaseRealtime1,0,200*PI,0,180));
  servo3.setAngle(map(F1_IN,0,4096,0,180));

  // Function Generator Right (sin)
  F2_IN = analogRead(F2_IN_Pin);
  temporalBaseRealtime2 = fmod(temporalBaseRealtime2 + ((F2_IN*stepMillis)/4096.0f),200.0*PI);
  ledcAnalogWrite(LEDC_CHANNEL_F2, (sin(temporalBaseRealtime2/(100.0))+1)*200.0, 400.0);
  servo2.setAngle(fmap(sin(temporalBaseRealtime2/100.0)+1,0,2,0,180));
  servo4.setAngle(fmap(F2_IN,0,4096,0,180));

       
  //  Read  excitation / inhibition levels
  leftMotorExcitation = analogRead ( LeftMotorExcitatoryPin ) ; 
  rightMotorExcitation = analogRead ( RightMotorExcitatoryPin ) ; 
  leftMotorInhibition = analogRead ( LeftMotorInhibitoryPin ) ; 
  rightMotorInhibition = analogRead ( RightMotorInhibitoryPin ) ;

  // Sample bumper state 
  leftBumperState = digitalRead ( LeftBumperPin ) ; 
  rightBumperState = digitalRead ( RightBumperPin ) ;

  // Escape Reflex
  if (leftBumperState)
    EscapeReflex (LeftBumperPin);
  else
    if (rightBumperState)
      EscapeReflex (RightBumperPin);
  
  // Drive equals excitation supressed by inhibition value
  leftMotorDrive = leftMotorExcitation - leftMotorInhibition;
  rightMotorDrive = rightMotorExcitation - rightMotorInhibition;


  int turnGain = (leftMotorDrive-rightMotorDrive);

  if (((leftMotorDrive + turnGain) * leftMotorDrive) > 0) //do not invert wheel rotation
    leftMotorDrive += turnGain;
  else
    leftMotorDrive = 0; //do not invert wheel rotation
    
  if (((rightMotorDrive - turnGain) * rightMotorDrive) > 0) //do not invert wheel rotation
    rightMotorDrive -= turnGain;
  else
    rightMotorDrive = 0;  //do not invert wheel rotation
  
  // Map excitation levels to motor drive with deadbands
  leftMotorDrive = MapWithDeadBand (1.5*leftMotorDrive, SensorMinValue, SensorMaxValue, LeftMinSpeed, LeftMaxSpeed);
  rightMotorDrive = MapWithDeadBand (1.5*rightMotorDrive, SensorMinValue, SensorMaxValue, RightMinSpeed, RightMaxSpeed);

  DriveMotors ( leftMotorDrive , rightMotorDrive );

  if (Serial) {
    writeSerial();
    ledcAnalogWrite(LEDC_CHANNEL_0, 511,511);
  } else
    ledcAnalogWrite(LEDC_CHANNEL_0, 0,511);
    
  //Keep active the communication MKR1000 & MKRMotorCarrier
  //Ping the samd11
  controller.ping();
  
  //delay (10);
}

int MapWithDeadBand(int value,int inputMin,int inputMax,int outMin, int outMax)
{
  // input deadBand:
  if ( (-inputMin <= value) && (value <= inputMin))
    return 0;
  else  
  // Scale input to output:
  {
    if (value > 0)
      return map(value, inputMin, inputMax, outMin, outMax);
    else
      return map(value, -inputMax, -inputMin, -outMax, -outMin);
  }
  //return value;
}

//  updates the drive on  the left and right motors
void  DriveMotors ( int leftDrive , int rightDrive)
{
      leftMotorCommand = -leftDrive;
      rightMotorCommand = rightDrive;
      
      pid1.setSetpoint(TARGET_VELOCITY, -leftDrive*10);
      pid2.setSetpoint(TARGET_VELOCITY, rightDrive*10);
      
      //M1.setDuty(-leftDrive);
      //M2.setDuty(rightDrive);  
}


      
// activates the escape reflex , given a bumper 
void EscapeReflex (int bumperPin )
{
  //  drive back for some amount of time
  DriveMotors(-LeftMaxSpeed , -RightMaxSpeed ) ;
  delay (500);

  //  if the left bumper  was activated , turn  to  the right
  if ( bumperPin == LeftBumperPin )
  {
    DriveMotors(LeftMaxSpeed , -RightMaxSpeed ) ;
  }
  // if the right bumper was activated , turn to the left
  if ( bumperPin == RightBumperPin )
  {
    DriveMotors(-LeftMaxSpeed , RightMaxSpeed ) ;
  }
  //  wait for turn to finish
  delay (300);
}

void behaviorFreeze()
{
  leftMotorDrive = 0;
  rightMotorDrive = 0;
}

void behaviorCircleRight()
{
  leftMotorDrive = 0;
  rightMotorDrive = 100;
}

void behaviorCircleLeft()
{
  leftMotorDrive = 100;
  rightMotorDrive = 0;
}

void writeSerial()
{
  Serial.print("LM+:");
  Serial.print(leftMotorExcitation);
  Serial.print("\t");
  Serial.print("RM+:"); 
  Serial.print(rightMotorExcitation);
  Serial.print("\t");
  
  Serial.print("LM-:");
  Serial.print(leftMotorInhibition);
  Serial.print("\t");
  Serial.print("RM-:");
  Serial.print(rightMotorInhibition);
  Serial.print("\t"); 

  Serial.print("E1a:");
  Serial.print(E1a);
  Serial.print("\t");
  Serial.print("E1b:"); 
  Serial.print(E1b);
  Serial.print("\t");
  Serial.print("E1c:"); 
  Serial.print(E1c);
  Serial.print("\t");

  Serial.print("Action:"); 
  Serial.print(currentAction*1000);
  Serial.print("\t");

  /* Serial.print("S1:");
  Serial.print(PWM_SUM_1);
  Serial.print("\t");

  Serial.print("E2a:");
  Serial.print(E2a);
  Serial.print("\t");
  Serial.print("E2b:"); 
  Serial.print(E2b);
  Serial.print("\t");
  Serial.print("E2c:"); 
  Serial.print(E2c);
  Serial.print("\t");
  
  Serial.print("S2:");
  Serial.print(PWM_SUM_2);
  Serial.print("\t");
  
  Serial.print("F1In:");
  Serial.print(F1_IN);
  Serial.print("\t"); 
  Serial.print("F2In:");
  Serial.print(F2_IN);
  Serial.print("\t"); 
*/
  Serial.print("LMotor:");
  Serial.print(leftMotorCommand);
  Serial.print("\t"); 
  Serial.print("RMotor:");
  Serial.println(rightMotorCommand);
}

float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

bool testBattery()
{
  float batteryVoltage = (float)battery.getConverted();
  
  //Reset to the default values if the battery level is lower than 11V
  if (batteryVoltage < 3) 
  {
    //  WARNING: LOW BATTERY ALL SYSTEMS DOWN
    M1.setDuty(0);
    M2.setDuty(0);
    M3.setDuty(0);
    M4.setDuty(0);
    return false;
  } else 
    return true;
}

void normalizeProbabilities()
{
  float sum = 0;
  for(int i=0; i<3; i++)
    sum += bProb[i];

  for(i=0; i<3; i++)
    bProb[i] /= sum;
}


int actionSelection()
{
  float sum = 0;
  float cumsum[3];
  for(i=0; i<3; i++)
  {
    cumsum[i] = sum;
    sum += bProb[i];   
  }

  float r = ((float)random(10000)) / 10000;

  for(int i=1; i<3; i++)
  {
    if(r < cumsum[i] )
    {
       return i-1;
    }
  }

  return 2;
}
