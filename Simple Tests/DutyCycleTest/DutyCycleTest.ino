
#include <MKRMotorCarrier.h>

int value = 80;

void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  initMotorController();

}



void loop() {
  // put your main code here, to run repeatedly:
  
  //Serial.println("First setpoint");
  //pid1.setSetpoint(TARGET_VELOCITY, -100);
  //pid2.setSetpoint(TARGET_VELOCITY, 100);
  M1.setDuty(-value);
  M2.setDuty(value);
  M3.setDuty(-value);
  M4.setDuty(value);

  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);
  //Serial.println("Second setpoint");
  
  //pid1.setSetpoint(TARGET_VELOCITY, 100);
  //pid2.setSetpoint(TARGET_VELOCITY, -100);
  
  M1.setDuty(0);
  M2.setDuty(0);
  M3.setDuty(0);
  M4.setDuty(0);

digitalWrite(LED_BUILTIN, HIGH);    // turn the LED off by making the voltage LOW   
  delay(1000);

  M1.setDuty(value);
  M2.setDuty(-value);
  M3.setDuty(value);
  M4.setDuty(-value);
  delay(1000);
  

}


void initMotorController()
{
  //Establishing the communication with the motor shield
  if (!controller.begin())
  {
    // Couldn't connect! Is the red led blinking? You may need to update the firmware with FWUpdater sketch
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
      delay(100);
    while (1);
  }

  Serial.print("MKR Motor Connected connected, firmware version ");
  Serial.println(controller.getFWVersion());
    
  // Reboot the motor controller; brings every value back to default
  controller.reboot();

  M1.setDuty(0);
  M2.setDuty(0); 
  M3.setDuty(0);
  M4.setDuty(0); 

}


