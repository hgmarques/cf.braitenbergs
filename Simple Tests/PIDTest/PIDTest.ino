
#include <MKRMotorCarrier.h>

int velocityFactor = 5;

void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  initMotorController();
}

void loop() {
  // put your main code here, to run repeatedly:
  
  Serial.println("First setpoint");
  pid1.setSetpoint(TARGET_VELOCITY, -100*velocityFactor);
  pid2.setSetpoint(TARGET_VELOCITY, 100*velocityFactor);
  
  delay(2000);
  Serial.println("Second setpoint");
  
  Serial.println("Third setpoint");
  pid1.setSetpoint(TARGET_VELOCITY, 100*velocityFactor);
  pid2.setSetpoint(TARGET_VELOCITY, -100*velocityFactor);
  
  delay(2000);
  controller.ping();
}


void initMotorController()
{
  //Establishing the communication with the motor shield
  if (!controller.begin())
  {
    // Couldn't connect! Is the red led blinking? You may need to update the firmware with FWUpdater sketch
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    while (1);
  }

  Serial.print("MKR Motor Connected connected, firmware version ");
  Serial.println(controller.getFWVersion());
  
  // Reboot the motor controller; brings every value back to default
  controller.reboot();

  M1.setDuty(0);
  M2.setDuty(0); 

  pid1.setControlMode(CL_VELOCITY);
  pid2.setControlMode(CL_VELOCITY);
  pid1.resetGains();
  pid2.resetGains();
  
  
  pid1.setGains(60, 10, 0); //50, 100, 0
  pid2.setGains(60, 10, 0);      
      
  pid1.setLimits(-100, 100);
  pid2.setLimits(-100, 100);
      
  pid1.setMaxAcceleration(40000);
  pid2.setMaxAcceleration(40000);
}


