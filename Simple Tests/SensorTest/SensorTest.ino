
// −−−−−−−−−−−---- Motor (Excitatory / Inhibitory) Pins −−−−−−--−−−−−− //

#define RightMotorExcitatoryPin    A3
#define LeftMotorExcitatoryPin     32
#define RightMotorInhibitoryPin    A2
#define LeftMotorInhibitoryPin     33

// −−−−−−−−−−−-------------- Bumper Pins −----------------−−−−−−−−−−−− //
#define RightBumperPin          17
#define LeftBumperPin           21


int rightMotorExcitation = 0;
int rightMotorInhibition = 0;
int leftMotorExcitation = 0;
int leftMotorInhibition = 0;

int leftBumper = 0;
int rightBumper = 0;

void setup() {
  Serial.begin(115200); 

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
}

void loop() {
  
  readPorts();
  printSerial();
  delay(1);  // delay in between reads for stability

}

void readPorts()
{
  rightMotorExcitation = analogRead(RightMotorExcitatoryPin);
  rightMotorInhibition = analogRead(RightMotorInhibitoryPin);
  leftMotorExcitation = analogRead(LeftMotorExcitatoryPin);
  leftMotorInhibition = analogRead(LeftMotorInhibitoryPin);

  rightBumper = digitalRead(RightBumperPin);
  leftBumper = digitalRead(LeftBumperPin);
}


void printSerial()
{
  Serial.print("LM+:");
  Serial.print(leftMotorExcitation);
  Serial.print("\t");
  Serial.print("RM+:"); 
  Serial.print(rightMotorExcitation);
  Serial.print("\t");
  
  Serial.print("LM-:");
  Serial.print(leftMotorInhibition);
  Serial.print("\t");
  Serial.print("RM-:");
  Serial.print(rightMotorInhibition);
  Serial.print("\t"); 

  Serial.print("LB:");
  Serial.print(leftBumper * 1000);
  Serial.print("\t");
  Serial.print("RB-:");
  Serial.print(rightBumper * 1000);
  Serial.print("\t"); 
  Serial.println();


}


